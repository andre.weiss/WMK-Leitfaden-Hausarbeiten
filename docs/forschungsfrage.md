# Forschungsfrage

* Bestimmt das Ziel und strukturiert die wissenschaftliche Arbeit
* Konkretisiert und grenzt das Thema ein, so dass es bearbeitbar wird.
* Sie kann folgende Elemente benennen:
    * __Untersuchungsgegenstand__ (Was soll untersucht werden?)
    * __Untersuchungsziel__ (Welche Frage soll beantwortet werden?)
    * __Untersuchungsmethode__ (Wie soll die Frage beantwortet werden?)
    * Räumliche oder zeitliche __Einschränkungen__

!!! example "Beispiel"
    „Inwieweit erfüllt das Onlineangebot des KIT die Bedürfnisse von Schüler*innen und Studierenden?“

* Eine Forschungsfrage sollte ...
    * explizit formuliert werden
    * als Frage formuliert sein
    * präzise formuliert sein
    * eng genug gefasst werden
    * möglichst früh formuliert werden
    * mit Dozent\*innen bzw. Betreuer\*innen besprochen werden
    * in Haus- und Abschlussarbeiten bereits in der Einleitung untergebracht werden