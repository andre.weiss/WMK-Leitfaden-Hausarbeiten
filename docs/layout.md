# Layout und Gestaltung

Allgemein gilt für Layout und Gestaltung der Arbeit: Übersichtlichkeit und Einheitlichkeit.

Das heißt, es ist auf eine einheitliche Schrift (Überschriften, Fließtext, Fußnoten), gleichbleibende Abstände der Absätze, usw. zu achten.

## Deckblatt

Das Deckblatt sollte folgende Angaben enthalten:

* Titel der Arbeit (inklusive Untertitel, falls vorhanden)
* Verfasser\*in
* Studienfach
* Fachsemester
* Matrikelnummer
* Anschrift
* E-Mailadresse (ggf. Telefonnummer) für Rückfragen
* Universität
* Institut
* Lehrveranstaltung
* Betreuer*\in
* Abgabedatum

Ein beispielhaftes Deckblatt sehen Sie im Folgenden:

![deckblatt](deckblatt.png){: loading=lazy }

## Inhaltsverzeichnis

* Das Inhaltsverzeichnis bekommt keine eigene Seitenzahl und wird – wie das Deckblatt – auch nicht mitgezählt.
* Folgt direkt nach dem Deckblatt.
* Gliederung mit Zahlen:
  
  ```
  	1. Kapitel
  	1.1 Unterkapitel
  	1.1.1 Unterunterkapitel
  	1.2 Unterkapitel
  	2. Kapitel
  	...
  ```

* Grundsätzlich gilt: Jede Untergliederung eines Oberpunktes erfordert mindestens zwei Unterpunkte. Ein Unterpunkt 2.1 setzt also immer die Existenz eines Unterpunktes 2.2 voraus. Allerdings sollte ein Oberpunkt nicht mehr als insgesamt fünf Unterpunkte haben.

## Gestaltung der einzelnen Seite

* Din-A4-Papier, auf PC mit gängigen Textverarbeitungsprogrammen (bspw. LaTeX, [LibreOffice Writer](https://www.libreoffice.org) oder Microsoft Word) geschrieben
* Etablierte Schriftart (serifenbetont oder serifenlos), z. B.: Times New Roman oder [Source Sans Pro](https://github.com/adobe-fonts/source-sans-pro)

??? danger "Schriftart und Normseite"
    Die gewählte Schriftart beeinflusst wie viel Text auf eine Seite passt. Sollten Sie für eine Hausarbeit eine Seitenanzahl und keine Zeichenanzahl vorgegeben bekommen, dann orientieren Sie sich bitte an 1500 Zeichen (inkl. Leerzeichen) pro Seite, um den Umfang abzuschätzen. Die 1500 Zeichen entsprechen einer [deutschen Normseite](https://de.wikipedia.org/wiki/Normseite#Nationale_Regelungen). Das heißt, wenn Ihre Hausarbeit 12 Seiten lang sein soll, dann dürfte Ihr Text maximal 12x1500 Zeichen, also 18.000 Zeichen umfassen.
	
	Sollten Sie unsicher sein, dann wenden Sie sich bitte direkt an die/den Dozent\*in.

* Schriftgröße: 12 Pt.
* doppelseitig in 1,5-fachem Zeilenabstand beschrieben

??? notice "Einseitiger oder zweiseitiger Druck"
    Doppelseitiger Druck hilft Ressourcen zu sparen und sollte deshalb bevorzugt werden. Es kann allerdings sein, dass Ihr\*e Betreuer\*in einseitigen Druck bevorzugt. Sprechen Sie dies deshalb im Zweifelsfall ab.

* Ränder:
	* rechts: 2,5 cm
	* links: 2,5 cm
* Blocksatz
* Seitenzahlen:
	* arabische Ziffern
	* beginnend mit der ersten Textseite, d.h.: Deckblatt und Inhaltsverzeichnis zählen nicht mit

## Fußnoten

* Schriftgröße: 10 Pt.
* Schriftart: einheitlich mit Schriftart des Fließtextes
* Zeilenabstand: Einfach
* Jede Fußnote beginnt mit Großschreibung und endet mit einem Punkt.

## Beschriftungen für Abbildungen und Tabellen

* Abbildungen und Tabellen müssen mit einer Beschriftung versehen werden.
* Diese sollte folgende Elemente beinhalten:
	* *Abbildung* bzw. *Tabelle*
	* Fortlaufende Nummer (Die Nummerierung von Abbildungen und Tabellen sollte getrennt erfolgen.)
	* Kurze Beschreibung des Inhalts
* Schriftgröße: 10 Pt.
* Schriftart: einheitlich mit Schriftart des Fließtextes

??? example "Beispiel"
    ![abbildung_beispiel](abbildung_beispiel.png){: loading=lazy }
	**Abbildung 1: Nutzung Sozialer Medien durch Wissenschaftler\*innen zur externen Wissenschaftskommunikation.**

## Verwendung doppelter und einfacher Anführungszeichen und Kursivierungen

* Zitate werden in doppelte Anführungszeichen gesetzt.
* Eigene Zitate, implizite Zitate, markierte Begriffe, uneigentliche Rede und Zitate innerhalb von Fremdzitaten werden mit einfachen Anführungszeichen versehen.
* Grundsätzlich gilt für die Verwendung einfacher Anführungszeichen und Kursivierungen, dass diese moderat zu verwenden sind.
* Werktitel (z.B. Film- und Romantitel) und Titel publizistischer Medien werden kursiv gesetzt, ebenso ist dies bei fremdsprachigen Ausdrücken möglich (Bsp.: _Süddeutsche Zeitung_, _Direct Cinema_ oder _Public Understanding_).
