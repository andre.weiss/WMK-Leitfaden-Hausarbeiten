# Aufbau

1. **Deckblatt**
	* Enthält notwendige Angaben zur Universität, zum Institut, Seminar und Betreuer\*in sowie zur/m Verfasser\*in der Arbeit.
	* → [Gestaltung](layout.md)
2. **Inhaltsverzeichnis**
	* Verzeichnet alle Kapitelüberschriften, sowohl der Haupt- als auch der Unterkapitel mit den entsprechenden Seitenangaben.
	* → [Gestaltung und Nummerierung der einzelnen Kapitel](layout.md)
3. **Einleitung**
	* Einführung des Themas
	* Relevanzbegründung
	* [Forschungsfrage](forschungsfrage.md)
	* Aufbau der Arbeit
3. **Forschungsstand / Theorien / Hypothesen**
	* Darstellung der bisherigen Forschung zum Thema
	* Theoretischer Rahmen
	* ggf. zu überprüfende Hypothesen
	* Untersuchungsdesign (Methoden)
4. **Hauptteil**
	* Ausführliche Darstellung der Ergebnisse
5. **Fazit**
	* Zusammenfassung der Ergebnisse
	* Bewertung und Einordnung der Ergebnisse
	* Einschränkungen
	* Ausblick (bspw. auf weiteren Forschungsbedarf)
6. **Literaturverzeichnis**
	* Angabe der verwendeten Quellen
	* Trennung in Primärquellen und Sekundärliteratur
	* Sortierung der Titel:
		* alphabetische Reihung nach dem ersten Familiennamen der/s ersten Autors\*in
		* chronologische Binnendifferenzierung, wenn ein/e Autor\*in mit mehreren Titeln zitiert wird
	* → [Details zur Angabe von Quellen und Literatur](literaturverzeichnis/index.md)
7. **Anhang**:
	* **ggf. Abbildungsverzeichnis**: Werden Abbildungen verwendet, so sind diese in einem eigenen Verzeichnis aufzuführen.
	* **ggf. Tabellenverzeichnis**
	* **ggf. Abkürzungsverzeichnis**: Abkürzungen und ihre Bedeutung
9. **Versicherung über die selbstständige Erstellung der Arbeit**:
	* [Vorlage als PDF zum Ausdrucken](https://wmk.itz.kit.edu/downloads/ich_versichere.pdf)
	* Der Hinweis kann am Ende der Arbeit (nach dem Quellenverzeichnis) oder vor dem Inhaltsverzeichnis angebracht werden.
	* Arbeiten ohne diese Erklärung werden nicht angenommen.
