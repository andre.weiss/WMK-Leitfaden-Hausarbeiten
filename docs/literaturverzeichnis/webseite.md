# Webseite

Darunter fallen ganze Webseiten, aber beispielsweise auch Blogeinträge oder andere Dokumente im Internet, die sich keiner anderen Kategorie zuordnen lassen.

## Typische Angaben

* Autor\*innen
* Erscheinungsjahr
* Titel des Artikels (inklusive Untertitel)
* Name der Webseite
* URL (mit Abrufdatum)

!!! note "Hinweis"
   Wenn möglich, sollten Sie für die _URL_ einen [Permalink](https://de.wikipedia.org/wiki/Permalink}) verwenden.

## Aufbau (APA)

Nachname, Initialen. (Jahr). Titel. URL mit Datum des Zugriffs und Name der Webseite.

## Beispiele

???+ example "APA 6"
    Klaue, M. (2015, April 22). Science-Slams: Die Wanderbühne der Wissenschaft. Abgerufen 29. Oktober 2020, von FAZ.NET: https://www.faz.net/1.3549279