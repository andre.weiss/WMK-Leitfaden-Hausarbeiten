# Film

## Typische Angaben
* Produzent\*innen
* Regisseur\*innen
* Erscheinungsjahr
* Titel des Films (inklusive Untertitel)
* Zusatz [Film]
* Land
* Studio
* URI

!!! tip "Tipp"
    Die Angaben zu Filmen können oftmals bei der [IMDb](https://www.imdb.com/) recherchiert werden.

## Aufbau (APA)

Nachname, Initialen. (Produzent\*in), & Nachname, Initialien. (Regisseur).  (Jahr). *Titel. Untertitel.* [Film]. Land: Studio. URI

!!! note "DVDs etc."
    Wenn auf eine spezielle Ausgabe eines Filmes verwiesen werden soll – bspw. eine DVD-Veröffentlichung – dann muss zum einen das Erscheinungsjahr entsprechend angepasst werden. Zum anderen sollte hinter dem Zusatz [Film] die Ausgabe bezeichnet werden, bspw. so [Film, DVD Veröffentlichung].

## Beispiele

???+ example "APA 6"
    Emmerich, R., Gordon, M. (Produzenten), & Emmerich, R. (Regisseur) (2004). *The Day After Tomorrow* [Film]. United States: 20th Century Fox.