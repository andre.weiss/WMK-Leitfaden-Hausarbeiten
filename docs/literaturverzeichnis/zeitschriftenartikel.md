# Zeitschriftenartikel

## Typische Angaben

* Autor\*innen
* Erscheinungsjahr
* Titel des Beitrags (inklusive Untertitel)
* Zeitschriftentitel
* Jahrgang
* Ausgabe
* Seitenangaben
* URI (sofern vorhanden; mit Abrufdatum bei \textit{URLs})

## Aufbau (APA)

Nachname, Initialen. (Jahr). Titel. Untertitel. In *Titel der Zeitschrift*, *Jahrgang* (Ausgabe), Seiten XX-YY. URI

## Beispiele

???+ example "APA 6"
    Humm, C., Schrögel, P., & Leßmöllmann. (2020). Feeling left out: Underserved audiences in science communication. *Media and Communication*, *8*(1), 164–176. https://doi.org/10.17645/mac.v8i1.2480