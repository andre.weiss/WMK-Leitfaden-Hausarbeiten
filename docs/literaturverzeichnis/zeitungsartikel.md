# Zeitungsartikel

## Typische Angaben

* Autor\*innen
* Erscheinungsdatum
* Titel des Artikels (inklusive Untertitel)
* Zeitungsname
* Jahrgang
* Ausgabe
* Seitenangaben
* URI (sofern vorhanden; mit Abrufdatum bei _URLs_)

## Aufbau (APA)

Nachname, Initialen. (Erscheinungsdatum): Titel. Untertitel. *Name der Zeitung*, Seiten XX-YY. URI

## Beispiele

???+ example "APA 6"
    Ribi, T. (2020, September 21). Bitte keine Kritik, da geht’s um Forschung! Wie Wissenschaftskommunikation zu Imagepflege wird – und warum. *Neue Zürcher Zeitung*, S. 25.