# Lexikon- / Enzyklopädieeintrag

## Typische Angaben

* Autor\*innen
* Erscheinungsjahr
* Titel des Eintrags (inklusive Untertitel)
* Herausgeber\*innen (Hrsg.)
* Titel des Lexikons (inklusive Untertitel)
* Band (sofern vorhanden)
* Auflage (außer bei der 1. Auflage)
* Seitenangaben
* Erscheinungsort
* Verlag
* URI (mit Abrufdatum)

## Aufbau (APA)

Nachname, Initialen. (Jahr). Titel. Untertitel. In Herausgeber\*innen (Hrsg.), *Titel des Lexikons* Band (Auflage, Seiten XX-YY). Erscheinungsort: Verlag.

## Beispiele

???+ example "APA 6"
    Kohring, M. (2013). Wissenschaftsjournalismus. In G. Bentele, H.-B. Brosius, & O. Jarren (Hrsg.), *Lexikon Kommunikations- und Medienwissenschaft* (2. Aufl., S. 374). Wiesbaden: Springer VS.