# Beitrag in einem Sammelband

## Typische Angaben

* Autor\*innen
* Erscheinungsjahr
* Titel des Beitrags (inklusive Untertitel)
* Herausgeber\*innen (Hrsg.)
* Titel (inklusive Untertitel)
* Erscheinungsort
* Verlag
* Seitenangaben
* URI (sofern vorhanden; mit Abrufdatum bei \textit{URLs})

## Aufbau (APA)

Nachname, Initialen. (Jahr). Titel des Beitrags. Untertitel des Beitrags. In Herausgeber\*innen (Hrsg.), *Titel des Sammelbands. Untertitel des Sammelbandes*, Erscheinungsort: Verlag, Seiten XX-YY. URI

## Beispiele

???+ example "APA 6"
    Schrögel, P., & Humm, C. (2020). 23. Science communication, advising, and advocacy in public debates. In A. Leßmöllmann, M. Dascal, & T. Gloning (Hrsg.), *Science Communication* (S. 485–514). Berlin, Boston: De Gruyter. https://doi.org/10.1515/9783110255522-023

