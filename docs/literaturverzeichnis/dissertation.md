# Dissertation

## Typische Angaben

* Autor\*in
* Erscheinungsjahr
* Titel (inklusive Untertitel)
* ”Dissertation”
* Universität
* Erscheinungsort
* URI (sofern vorhanden; mit Abrufdatum bei _URLs_)

## Aufbau (APA)

Nachname, Initialen. (Jahr). _Titel. Untertitel._ (Dissertation, Universität, Erscheinungsort). URI

### Beispiele

???+ example "APA 6"
    Shults, A. (2008). *Objectives and Tools of Science Communication in the Context of Globalization* (Universität des Saarlandes). Universität des Saarlandes, Saarbrücken. Abgerufen von https://core.ac.uk/download/pdf/10697609.pdf