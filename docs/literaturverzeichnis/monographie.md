# Monographie

Monographien sind Bücher, die von einem oder mehreren Autor\*innen selbst verfasst wurden.

## Typische Angaben

* Autor\*in
* Erscheinungsjahr
* Titel (inklusive Untertitel)
* Auflage (außer bei der 1. Auflage)
* Erscheinungsort
* Verlag
* URI (sofern vorhanden)

## Aufbau (APA)

Nachname, Initialen. (Jahr). _Titel. Untertitel._ (Auflage). Erscheinungsort: Verlag.

## Beispiele

???+ example "Beispiel (APA 6)"
    Kohring, M. (2005). _Wissenschaftsjournalismus. Forschungsüberblick und Theorieentwurf_ (2. Aufl.). Konstanz: UVK

??? example collapsible "Beispiel (MLA 8)"
    Kohring, Matthias. _Wissenschaftsjournalismus. Forschungsüberblick und Theorieentwurf._ 2. Aufl., UVK, 2005.
