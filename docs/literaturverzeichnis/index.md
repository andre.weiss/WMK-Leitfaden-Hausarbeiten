# Quellen- und Literaturverzeichnis

## Gliederung

Sinn und Zweck des Quellen- und Literaturverzeichnisses ist es, die in der Hausarbeit verwendeten Quellen und Literatur nachzuweisen. Deshalb muss aus diesen eindeutig hervorgehen, welches Werk genau gemeint ist. Also beispielsweise welche Auflage eines Buches.

* Das Quellenverzeichnis untergliedert sich in Primärquellen als Gegenstand der Untersuchung (z.B. analysierte Filme, Webvideos oder Zeitschriften) und Sekundärquellen bzw. -literatur, also Literatur über die untersuchten Primärquellen.
* Internetquellen sind entweder Primär- oder Sekundärquellen und werden nicht gesondert aufgeführt.
* Gegebenenfalls enthält das Quellenverzeichnis zusätzlich ein Abbildungsverzeichnis.

## Sortierung

* Die Literaturangaben sind __alphabetisch__ zu sortieren.
* Bei mehreren Werken derselben Autor\*innen sind diese __chronologisch__ zu sortieren.
*  Bei mehreren Werken derselben Autor\*innen im gleichen Jahr wird hinter das Jahr ein Buchstabe angefügt (bspw. 2018a, 2018b etc.).

## Angaben im Literaturverzeichnis

!!! danger "Hinweis"
    Geben Sie nur Werke an, deren Verwendung in der Arbeit durch einen Beleg nachgewiesen ist.

Es gibt zahlreiche  verschiedene Standards, die regeln, welche Angaben wie im Literaturverzeichnis aufzunehmen sind. Die folgenden Beispiele folgen dem sogenannten [APA Style](https://apastyle.apa.org/), dem in den Sozialwissenschaften weitverbreiteten Zitierstil der *Amercian Psychological Association*, in der 6. Ausgabe.

!!!note "Hinweis"
    Kürzlich neu erschienen ist die 7. Ausgabe, diese verzichtet unter anderem auf die Nennung des Erscheinungsortes.
