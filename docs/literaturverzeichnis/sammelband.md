# Sammelband

Sammelbände sind Bücher, die von einer oder mehreren Personen herausgegeben wurden und in denen sich Aufsätze verschiedener Autor\*innen befinden.

Für die Angabe einzelner Beiträge aus einem Sammelband besuchen Sie bitte die [entsprechende Seite](sammelband_beitrag.md).

## Typische Angaben

* Herausgeber\*innen (Hrsg.)
* Erscheinungsjahr
* Titel (inklusive Untertitel)
* Erscheinungsort
* Verlag
* URI (sofern vorhanden; mit Abrufdatum bei *URLs*)

## Aufbau (APA)

Nachname, Initialen. (Hrsg.) (Jahr). *Titel. Untertitel.* Erscheinungsort: Verlag. URI

## Beispiele

???+ example "APA 6"
    Bonfadelli, H., Fähnrich, B., Lüthje, C., Milde, J., Rhomberg, M., & Schäfer, M. S. (Hrsg.). (2017). *Forschungsfeld Wissenschaftskommunikation*. Wiesbaden: VS Verlag für Sozialwissenschaften. Abgerufen von https://www.springer.com/de/book/9783658128975
