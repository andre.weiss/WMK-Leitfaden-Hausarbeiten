# Videos

Darunter fallen beispielsweise Videos auf *YouTube* oder *Vimeo*. Klassische Filme sollten hingegen wie [Filme](film.md) angegeben werden.

## Typische Angaben

* Name Uploader\*in bzw. Kanal
* Erscheinungsjahr (ggf. aus Uploaddatum ermitteln)
* Titel des Videos
* Name der Videoplattform
* URL (mit Abrufdatum)

## Aufbau (APA)

Nachname, Initialen. (Jahr). Titel. _Name der Videoplattform_. URL mit Datum des Zugriffs.

## Beispiel

???+ example "APA 6"
    NiksDa. (2019, Januar 18). Kein Thema (prod. Dalton) | #EarthOvershootDay. *YouTube*. Abgerufen 21. Juni 2021 von: https://youtu.be/Fwz4T63IHmA