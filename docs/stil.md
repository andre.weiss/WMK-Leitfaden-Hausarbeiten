# Stil

* Achten Sie auf **Rechtschreibung** und **Grammatik**.
* Benutzen Sie (passende) **Fachwörter**.
* Vermeiden Sie **„ich“**.

??? example "Beispiel"
    **Schlecht**: „Ich habe mir die Spektrum angeschaut.“
    
    **Besser**: „Analysiert wurde das Monatsmagazin Spektrum.“

* Vermeiden Sie **unbegründete Wertungen**.

??? example "Beispiel"
    **Schlecht**: „Die Studie von Meier war leider schwer zu lesen und ist schlecht.“

    **Besser**: „Die Studie von Meier vernachlässigt die Dimension der Multimodalität und damit einen zentralen Aspekt neuer Medien (vgl. Bucher, Schumacher, 2012).“

* Versuchen Sie einen **roten Faden** beizubehalten.
* Vermeiden Sie **Exkurse**.
* Vermeiden Sie **unpräzise Angaben** („nahezu“, „fast“, „einige“ ...)
* Siehe auch: [Anleitung für schlechte Studienarbeiten](https://www.wiwi.uni-wuerzburg.de/fileadmin/12020700/user_upload/Publikationen/Anleitung_fuer_schlechte_Studienarbeiten.pdf)