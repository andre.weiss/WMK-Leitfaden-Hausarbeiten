# Grundsätzliche Hinweise

Ziel einer Hausarbeit ist die selbständige und kritische Erarbeitung eines Themas unter Zuhilfenahme der verfügbaren Forschungsliteratur. Gezeigt werden soll dabei die Fähigkeit, fachspezifische Themen problembewusst, methodengeleitet und formal richtig zu bearbeiten.

!!! quote "Wissenschaftliche Redlichkeit ([LHG BW, § 3 Abs. 5](http://www.landesrecht-bw.de/jportal/?quelle=jlink&docid=jlr-HSchulGBWrahmen&psml=bsbawueprod.psml&max=true))"
    Alle an der Hochschule wissenschaftlich Tätigen sowie die Studierenden sind zu wissenschaftlicher Redlichkeit verpflichtet. Hierzu sind die allgemein anerkannten Grundsätze guter wissenschaftlicher Praxis einzuhalten. Ein Verstoß hiergegen liegt insbesondere vor, wenn in einem wissenschaftserheblichen Zusammenhang vorsätzlich oder grob fahrlässig Falschangaben gemacht werden, geistiges Eigentum anderer verletzt oder die Forschungstätigkeit Dritter erheblich beeinträchtigt wird. Im Rahmen der Selbstkontrolle in der Wissenschaft stellen die Hochschulen Regeln zur Einhaltung der allgemein anerkannten Grundsätze guter wissenschaftlicher Praxis und zum Umgang mit wissenschaftlichem Fehlverhalten auf.

## Verfassen einer wissenschaftlichen Hausarbeit

* argumentierender Stil; keine Zitatsammlungen!
* eigene Urteile, aber keine Bekenntnisse!
* Verwendung der Forschungsliteratur, aber keine bloße Wiederholung!
* Sprache: sachlich, klar, unpathetisch; keine Umgangssprache