# Quellenwiedergabe (Zitattechnik)

## Allgemeine Hinweise zur Quellenwiedergabe

* **Quellenangaben** sind notwendig bei:
    1. **Wörtlichen Zitaten**
    :   Kennzeichnen durch doppelte Anführungszeichnen zu Beginn und Schluss („ ... “).
    2. **Indirekten Wiedergaben von Gedanken, Meinungen etc. anderer Autor*innen (Paraphrasen)**
    :   Kann zusätzlich gekennzeichnet werden mit der Abkürzung ‚Vgl.’ für vergleiche.
    3. **Wiedergabe von Daten und Aussagen**, die nicht als Allgemeinwissen gelten können.
* **Ellipsen (Auslassungen) bzw. eigene Ergänzungen/grammatische Anpassungen** im Zitat werden durch eckige Klammern gekennzeichnet, z.B. bei Auslassungen: […].
* Die **Interpunktion/Orthographie** des zitierten Textes ist beizubehalten; grobe, vor allem sinnentstellende Fehler sind zu kennzeichnen durch: [sic!].
* **Seitenangabe für zwei Seiten**: f. (für: ‚und folgende Seite’); mehrere folgende Seiten sollten folgendermaßen angegeben werden: 101-105 (die Verwendung von ff. ist zu vermeiden).
* **Quellenarbeit**: Es sollten nur Primärzitate verwendet werden, da der direkte Zugriff auf die Quelle gewährleistet werden muss; sekundäre oder gar tertiäre Zitierungen sind zu vermeiden. Sollte ein Sekundärzitat nicht vermieden werden können, ist die Primärquelle dennoch anzugeben, gefolgt von der Angabe der Sekundärquelle (eingeleitet mit ‚zitiert nach’).
* **Zitierweisen**: Es gibt grundsätzlich zwei unterschiedliche Möglichkeiten Belege im Text anzugeben. Zum einen in Fuß- oder Endnoten, dies ist die sogenannte [deutsche Zitierweise](#deutsche-zitierweise-funoten). Zum anderen in Klammern im Fließtext, dies ist die sogenannte [amerikanische Zitierweise](#amerikanische-zitierweise-im-text).

!!! danger "Vorgaben von Dozierenden"
    Prinzipiell sind beide Zitierweisen möglich und man kann sich für eine von beiden entscheiden. Allerdings können Lehrende auch eine bestimmte Zitierweise vorgeben.

## Deutsche Zitierweise (Fußnoten)

* Bei der deutschen Zitierweise steht die Quellenangabe in einer Fußnote am Ende der jeweiligen Seite.
* Beim ersten Beleg eines Titels muss dieser in seiner Langform angeben werden.
    * Die/der Verfasser\*in wird dabei in der Reihenfolge Vorname Nachname genannt, für das Literaturverzeichnis gilt die umgekehrte Reihenfolge.
* Nachdem der Titel einmal in seiner Langform aufgetaucht ist, genügt danach die Kurzform.
* Wird unmittelbar hintereinander auf dieselbe Quelle Bezug genommen, genügt *‘ebd.’* oder *‘ibid.’* und die Seitenangabe (auf die Seitenangabe ist zu verzichten, wenn sie mit derjenigen des unmittelbar vorangegangenen Zitats übereinstimmt): Ebd., S. 4.

??? example "Deutsche Zitierweise (Langform)"
    „Die Differenz von Wissen und Nichtwissen kennzeichnet moderne Gesellschaften. Infrage steht allerdings, ob der Abstand kleiner oder größer geworden ist im Laufe der Jahrhunderte. Kann der Mensch in modernen Gesellschaften tatsächlich Nützliches von Unnützlichem unterscheiden? Kann er Ambivalenzen und Unsicherheiten besser aushalten als seine Vorfahren? Bedingt nicht Wissen Unwissen in gleichem, vielleicht sogar in höherem Maße? Was ist Wissen? Schafft Wissenschaft Wissen?“<sup>1</sup>
    
    1 B. Dernbach, C. Kleinert, & H. Münder (2012). Einleitung: Die drei Ebenen der Wissenschaftskommunikation. In B. Dernbach, C. Kleinert, & H. Münder (Hrsg.), Handbuch Wissenschaftskommunikation (S. 1–15). Wiesbaden: VS Verlag für Sozialwissenschaften. S. 4.

??? example "Deutsche Zitierweise (Kurzform)"
    „Die Differenz von Wissen und Nichtwissen kennzeichnet moderne Gesellschaften.“<sup>1</sup>

    1 Dernbach, Kleinert, & Münder (2012): Einleitung. Drei Ebenen der Wissenschaftskommunikation, S. 4.

## Amerikanische Zitierweise (im Text)

* Bei der amerikanischen Zitierweise steht die Quellenangabe in Klammern im Fließtext.
* Diese Ziterweise wird manchmal auch als *Harvard Zitierweise* bezeichnet.
* Es werden nur folgende Angaben gemacht: *Autor\*in*, *Jahr* und *Seitenzahl(en)*

??? example "Amerikanische Zitierweise"
    „Die Differenz von Wissen und Nichtwissen kennzeichnet moderne Gesellschaften.“ (Dernbach, Kleinert, & Münder, 2012, 4)

## Filmdialoge

* Alle Hinzufügungen bei Filmdialogen, z.B. die Namen der sprechenden Figuren, müssen in eckige Klammer gesetzt werden.
* Es muss der Timecode (TC) angegeben werden, um Zeitpunkt und Dauer der Szene im Film kenntlich zu machen.

??? example "Beispiel"
    [Irene Adler]: Wissen Sie, was das Problem bei einer Verkleidung ist, Mr. Holmes? Was man auch versucht, es ist immer ein Selbstportrait.

    [Sherlock Holmes]: Denken Sie, ich bin ein Pfarrer mit blutendem Gesicht?

    [Irene Adler]: Ich denke, Sie sind soziopathisch, wahnhaft und glauben an eine höhere Macht. In Ihrem Fall an sich selbst.<sup>1</sup>
    
    1 Sherlock. A Scandal in Belgravia, R.: Paul McGuigan, Staffel 2, Folge 1, England 2012, TC: 00:25:28-00:25:46.

## Abbildungen

* Werden in der Arbeit Abbildungen verwendet, die nicht von Ihnen selbst erstellt wurden, so müssen diese wie Textstellen belegt werden.
* Screenshots aus Filmen werden wie Abbildungen behandelt, müssen also auch mit einer Quellengabe versehen werden.
