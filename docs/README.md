# Leitfaden zur Erstellung einer wissenschaftlichen Hausarbeit

Studiengang "Wissenschaft – Medien – Kommunikation”

Autor: [Christian Humm M.A.](https://www.c-m-l.net/)

!!! note "Hinweis"
    Dieser Leitfaden befindet sich noch im Aufbau und wird stetig erweitert. Bitte melden Sie etwaige Fehler entweder direkt im [GitLab Repository](https://gitlab.kit.edu/andre.weiss/WMK-Leitfaden-Hausarbeiten) oder per [E-Mail](mailto:andre.weiss@kit.edu).

## ToDo

* [ ] Literangaben
	* [ ] Fernsehsendungen
	* [ ] Podcasts
	* [X] Videos (YouTube, Vimeo etc.)
* [ ] Beispiele
	* [X] APA 6
	* [ ] APA 7
	* [ ] MLA 8
* [ ] Layout
	* [ ] Farben
	* [ ] Logo
